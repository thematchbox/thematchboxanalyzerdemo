package com.thematchbox;

import be.thematchbox.api.MatchBoxServices;
import be.thematchbox.api.data.*;
import be.thematchbox.common.Language;

import java.io.File;

public class AnalyzerDemoTheMatchBox {
    public static void main(String[] args) {
        //
        // One can use several paths to search for a license. If multiple licenses are found, the first one is chosen.
        // A file is considered a license file when it starts with 'theMatchBox" and ends with '.lic' (e.g. theMatchBox-demo.lic)
        //
        String userHome = System.getProperty("user.home");
        String localPath = userHome + File.separator + ".thematchbox";

        String dummyPath = userHome + File.separator + "foo" + File.separator + "bar" + File.separator + "something";
        String fullPath = dummyPath + File.pathSeparator + localPath;

        //
        // Create one single service class as a singleton e.g. using IOC-containers like Spring or Guice.
        // One can safely use it in multiple threads. All calls are stateless, but intensive caching is used in thread-local storage.
        // To get the best performance, reuse threads as much as possible.
        // The thread local caches are references by using soft references, so the JVM will dispose long time unused caches when
        // in need of more memory.
        //
        // It is possible to manually set the cache sizes. If a cache size <= 0, no caching is used.
        //
        // There are two types of caches:
        // - main cache
        // - spell checker cache
        //
        // The main cache is the most important one to keep processing speeds high enough and has the smallest memory footprint.
        //
        MatchBoxServices services = new MatchBoxServices(20000, 0, 5, fullPath);

        //
        // Call the analyze method with the desired language and features.
        //
        // Available languages are DUT, FRE and ENG
        //
        // Useful Feature Sets are
        // - LEMMA_SPLIT: 'lemmatizes' words (e.g. plurals, feminine to masculine) and splits compound words.
        // - LEMMA_SPLIT_SPELL: 'lemmatizes' words (e.g. plurals, feminine to masculine), splits compound words and tries to correct typos.
        //
        //
        Document document = services.analyze("Hoofdverpleegkundgen", Language.DUT, Features.LEMMA_SPLIT_SPELL);
        for (Token token : document.tokens) {
            System.out.println("Token " + token.string + "; type " + token.type.name());
            for (TokenPart part : token.parts) {
                MatchBoxString originalPart = document.originalString.subString(part.from, part.to);
                System.out.println("\t" + part.string + (part.corrected ? " (corrected)" : "") + ", original: " + originalPart);
            }
        }
    }
}
