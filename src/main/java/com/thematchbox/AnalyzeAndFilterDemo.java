package com.thematchbox;

import be.thematchbox.api.MatchBoxServices;
import be.thematchbox.api.data.*;
import be.thematchbox.api.filters.*;
import be.thematchbox.common.Language;

import java.io.File;
import java.util.List;

public class AnalyzeAndFilterDemo {
    public static void main(String[] args) {
        String userHome = System.getProperty("user.home");
        String localPath = userHome + File.separator + ".thematchbox";

        String dummyPath = userHome + File.separator + "foo" + File.separator + "bar" + File.separator + "something";
        String fullPath = dummyPath + File.pathSeparator + localPath;

        MatchBoxServices services = new MatchBoxServices(5, fullPath);

        String text = "This is my email address: joeri.mesens@thematchbox.be.";

        Document document = services.analyze(text, Language.ENG, Features.LEMMA_SPLIT_SPELL);
        dumpTokens(document.tokens, false);

        System.out.println();
        System.out.println("Filtered on token type");
        TokenFilter filter = TokenTypeFilter.ACCEPT_TEXT_ONLY_FILTER;
        List<Token> textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, false);

        System.out.println();
        System.out.println("Filtered on token type: keep email");
        filter = new TokenTypeFilter(FilterType.ACCEPT, TokenType.EMAIL);
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on token type: reject email");
        filter = new TokenTypeFilter(FilterType.REJECT, TokenType.EMAIL);
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on word type");
        filter = WordTypeFilter.REJECT_STOP_WORDS_FILTER;
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on word type, keep nouns");
        filter = new WordTypeFilter(FilterType.ACCEPT, WordType.NOUN);
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on word type, reject nouns");
        filter = new WordTypeFilter(FilterType.REJECT, WordType.NOUN);
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on analyzed strings, accept mailadres");
        filter = StringTokenFilter.filterOnAnalyzedString(FilterType.ACCEPT, "mailadres");
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on analyzed strings, reject mailadres");
        filter = StringTokenFilter.filterOnAnalyzedString(FilterType.REJECT, "mailadres");
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on original strings, keep Dit");
        filter = StringTokenFilter.filterOnOriginalString(FilterType.ACCEPT, document, "Dit");
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Filtered on original strings, reject Dit");
        filter = StringTokenFilter.filterOnOriginalString(FilterType.ACCEPT, document, "Dit");
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);

        System.out.println();
        System.out.println("Chained filters on token types and word types");
        filter = new ChainedFilter(TokenTypeFilter.ACCEPT_TEXT_ONLY_FILTER, WordTypeFilter.REJECT_STOP_WORDS_FILTER);
        textTokens = filter.filter(document.tokens);
        dumpTokens(textTokens, true);


    }

    private static void dumpTokens(List<Token> tokens, boolean full) {
        dumpTokens(tokens.toArray(new Token[tokens.size()]), full);
    }

    private static void dumpTokens(Token[] tokens, boolean full) {
        if (full) {
            for (Token token : tokens) {
                System.out.println("Token " + token.string + "; type " + token.type.name());
                for (TokenPart part : token.parts) {
                    StringBuilder builder = new StringBuilder();
                    builder.append("\t").append(part.string).append(" ");
                    if (part.wordTypes.length > 0) {
                        builder.append("(");
                        WordType[] wordTypes = part.wordTypes;
                        for (int i = 0; i < wordTypes.length; i++) {
                            WordType wordType = wordTypes[i];
                            if (i > 0) {
                                builder.append(", ");
                            }
                            builder.append(wordType.name());
                        }
                        builder.append(")");
                    }

                    if (part.corrected) {
                        builder.append(" corrected");
                    }
                    System.out.println(builder.toString());
                }
            }
        } else {
            StringBuilder builder = new StringBuilder();
            for (Token token : tokens) {
                builder.append("[").append(token.string).append("]");
            }
            System.out.println(builder);
        }
    }
}
